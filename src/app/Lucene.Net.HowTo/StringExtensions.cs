﻿namespace Lucene.Net.HowTo
{
	public static class StringExtensions
	{
		public static int? TryParseInt(this string s)
		{
			try
			{
				int val;
				if (!int.TryParse(s, out val))
				{
					return null;
				}
				return val;
			}
			catch
			{
				return null;
			}
		}
	}
}