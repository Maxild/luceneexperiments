﻿using System;
using System.IO;
using System.Reflection;

namespace Lucene.Net.HowTo
{
	public static class Utils
	{
		public static string GetRootDir()
		{
			string codeBaseDirectory = Assembly.GetExecutingAssembly().GetCodeBaseDirectory();

			// ..\..\..\..\..\codeBaseDirectory
			var root =
				Path.GetDirectoryName(
					Path.GetDirectoryName(
						Path.GetDirectoryName(
							Path.GetDirectoryName(
								Path.GetDirectoryName(codeBaseDirectory)))));

			if (string.IsNullOrEmpty(root))
			{
				throw new InvalidOperationException(@"'..\..\..\..\..\codeBaseDirectory' does not exist.");
			}
			return root;
		}
	}
}