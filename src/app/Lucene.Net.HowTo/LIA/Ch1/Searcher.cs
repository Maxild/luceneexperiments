﻿using System;
using System.IO;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;

namespace Lucene.Net.HowTo.LIA.Ch1
{
	public class Searcher
	{
		public static int Main(string[] args)
		{
			string indexDir = args.Length == 1 ? args[0] : Path.Combine(Utils.GetRootDir(), "indexes", "Licenses");

			Console.WriteLine("Searcher is using index located at:");
			Console.WriteLine("    " + indexDir);
			
			while (true)
			{
				Console.WriteLine();
				Console.Write("Enter Lucene Query >> ");
				string queryString = Console.ReadLine();

				if (string.IsNullOrEmpty(queryString) || queryString.Trim() == "q")
				{
					break;
				}

				Search(indexDir, queryString);
			}

			return 0;
		}

		public static void Search(string indexDir, string q)
		{
			using (var indexSearcher = new IndexSearcher(FSDirectory.Open(indexDir)))
			{
				var parser = new QueryParser(Util.Version.LUCENE_30, "contents", new StandardAnalyzer(Util.Version.LUCENE_30));
				Query query = parser.Parse(q);

				TopDocs hits = null;
				int millisecinds = Timer.Execute(() => hits = indexSearcher.Search(query, 10)).Milliseconds;

				Console.WriteLine("Found " + hits.TotalHits + " document(s) (in " + millisecinds +
								  " milliseconds) that matched query '" +
								  q + "':");

				foreach (ScoreDoc scoreDoc in hits.ScoreDocs)
				{
					Document doc = indexSearcher.Doc(scoreDoc.Doc);
					Console.WriteLine(doc.Get("fullpath"));
				}
			}
		}
	}
}