﻿using System;
using System.IO;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;

namespace Lucene.Net.HowTo.LIA.Ch1
{
	public class Indexer : IDisposable
	{
		readonly static SimpleFSLockFactory _lockFactory = new SimpleFSLockFactory();

		public static int Main(string[] args)
		{
			string indexDir, dataDir;
			if (args == null || args.Length == 0)
			{
				string root = Utils.GetRootDir();
				indexDir = Path.Combine(root, "indexes", "Licenses");
				dataDir = Path.Combine(root, "data", "Licenses");
			}
			else
			{
				if (args.Length != 2)
				{
					Console.WriteLine("Usage: program [<index dir> <data dir>]");
					return -1;
				}

				indexDir = args[0];
				dataDir = args[1];
			}

			int numIndexed = 0;
			int milliseconds = Timer.Execute(() =>
				{
					using (var indexer = new Indexer(indexDir))
					{
						numIndexed = indexer.Index(dataDir);
					}
				}).Milliseconds;

			Console.WriteLine("Indexing " + numIndexed + " files took "
							  + milliseconds + " milliseconds");

			return 0;
		}

		private readonly IndexWriter _writer;

		public Indexer(string indexDir)
		{
			var directory = FSDirectory.Open(new DirectoryInfo(indexDir), _lockFactory);
			_writer = new IndexWriter(directory, new StandardAnalyzer(Util.Version.LUCENE_30), true,
									  IndexWriter.MaxFieldLength.UNLIMITED);
		}

		public void Dispose()
		{
			if (_writer != null)
			{
				_writer.Dispose();
			}
		}

		/// <summary>
		/// Build new index
		/// </summary>
		/// <param name="dataDir">Directory where txt files are found</param>
		/// <returns>The number of documents in the index</returns>
		public int Index(String dataDir)
		{
			new DirectoryInfo(dataDir).EnumerateFiles("*.txt").Each(IndexFile);
			_writer.Optimize();
			return _writer.MaxDoc();
		}

		private void IndexFile(FileInfo file)
		{
			Console.WriteLine("Indexing " + file.Name);
			Document doc = GetDocument(file);
			_writer.AddDocument(doc);
		}

		private static Document GetDocument(FileInfo file)
		{
			var doc = new Document();

			TextReader contents = new StreamReader(file.OpenRead());

			doc.Add(new Field("contents", contents));
			doc.Add(new Field("filename", file.Name,
						Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.Add(new Field("fullpath", file.FullName,
						Field.Store.YES, Field.Index.NOT_ANALYZED));

			return doc;
		}
	}
}