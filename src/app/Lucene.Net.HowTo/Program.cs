﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Mono.Options;

namespace Lucene.Net.HowTo
{
	class Program
	{
		private static int _verbose;
		public static bool Verbose
		{
			get { return _verbose > 0; }
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Lucene Experiments by Morten Maxild, Copyright 2013");

			// Mono.Options.OptionSet is built upon a key/value table, where the
			// key is an option format string and the value is a delegate that is 
			// invoked when the format string is matched.

			// Each '|'-delimited name is an alias for the associated action.  If the
			// format string ends in a '=', it has a required value. If the format
			// string ends in a ':', it has an optional value. If neither '=' or ':'
			// is present, no value is supported. `=' or `:' need only be defined on one
			// alias, but if they are provided on more than one they must be consistent.

			// The `name' used in the option format string does NOT include any leading
			// option indicator, such as '-', '--', or '/'.  All three of these are
			// permitted/required on any named option.

			string book = null;
			int? chapter = null;
			string module = null;

			OptionSet p = null;
			p = new OptionSet
				{
					{"v|versbose", v => _verbose += 1},
					{"h|?|help", v => ShowHelp(p)},
					{ "b:|book", v => book = v },
					{ "ch:|chapter", v =>
						{
							if (!string.IsNullOrEmpty(v))
							{
								chapter = int.Parse(v);
							}
						}
					},
					{ "m:|module", v => module = v}
				};

			List<string> extra = p.Parse(args);

			switch (book.ToLowerInvariant())
			{
				case "lia":
					if (chapter == null)
					{
						Console.Write("Enter Chapter >> ");
						chapter = Console.ReadLine().TryParseInt();
					}
					if (string.IsNullOrEmpty(module))
					{
						Console.Write("Enter Module >> ");
						module = Console.ReadLine();
					}
					LiaDispatcher(chapter, module, extra.ToArray());
					break;
				case "instant":
					InstantDispatcher();
					break;
				default:
					Console.WriteLine("Not Implemented");
					break;
			}
		}

		static void LiaDispatcher(int? chapter, string module, string[] args)
		{
			chapter = chapter ?? 1;
			module = string.IsNullOrEmpty(module) ? "indexer" : module;

			Type entryModule =
				Assembly.GetExecutingAssembly()
				        .GetTypes()
				        .First(t =>
				               (t.Namespace ?? string.Empty).Contains(".LIA.Ch" + chapter) &&
				               t.Name.Equals(module, StringComparison.OrdinalIgnoreCase));

			entryModule.GetMethod("Main", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)
			           .Invoke(null, new object[] {args});
		}

		static void InstantDispatcher()
		{
			Console.WriteLine("Not implemented");
		}

		static void ShowHelp(OptionSet p)
		{
			var sb = new StringBuilder();
			TextWriter writer = new StringWriter(sb);
			p.WriteOptionDescriptions(writer);
			Console.WriteLine(sb.ToString());
		}
	}
}