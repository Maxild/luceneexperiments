﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Lucene.Net.HowTo
{
	public static class EnumerableExtensions
	{
		[DebuggerStepThrough]
		public static IEnumerable Each(this IEnumerable values, Action<object> eachAction)
		{
			foreach (var item in values)
			{
				eachAction(item);
			}
			return values;
		}


		[DebuggerStepThrough]
		public static IEnumerable<T> Each<T>(this IEnumerable<T> values, Action<T> eachAction)
		{
			foreach (var item in values)
			{
				eachAction(item);
			}
			return values;
		}

		[DebuggerStepThrough]
		public static void EachWord(this string phrase, Action<string> a)
		{
			phrase.Split(new [] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
			      .Each(a);
		}
	}
}