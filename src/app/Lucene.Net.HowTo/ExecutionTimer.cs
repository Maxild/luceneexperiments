﻿using System;
using System.Diagnostics;

namespace Lucene.Net.HowTo
{
	public static class Timer
	{
		public static TimeSpan Execute(Action action)
		{
			var timer = Stopwatch.StartNew();
			action.Invoke();
			timer.Stop();
			return timer.Elapsed;
		}
	}
}